# Collection

## About

A repository of supporting assets (artworks, awarding conditions, consumer rules etc.) for the badges available for being awarded to the community members, which can be read from and updated by the interactions with the Accolades API entity and the Liberation entity as well as by admins having relevant accesses to the repository

## Read more

1. https://fedora-arc.readthedocs.io/en/latest/badges/prop_rewrite_entities.html#internal-entities
